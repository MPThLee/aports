# Contributor: prspkt <prspkt@protonmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-importlib-metadata
pkgver=5.1.0
pkgrel=0
pkgdesc="Read metadata from Python packages"
url="https://github.com/python/importlib_metadata"
arch="noarch"
license="Apache-2.0"
depends="
	py3-zipp
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-pyfakefs
	py3-pytest
	python3-tests
	"
source="https://pypi.python.org/packages/source/i/importlib_metadata/importlib_metadata-$pkgver.tar.gz"
#options="!check" # Tests fail to find module "test"
builddir="$srcdir/importlib_metadata-$pkgver"

export SETUPTOOLS_SCM_PRETEND_VERSION="$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest tests
}

package() {
	python3 -m installer \
		-d "$pkgdir" \
		dist/importlib_metadata-$pkgver-py3-none-any.whl
}

sha512sums="
9e3d42771f4fcbe334c1b4044415e91abd677f99adbe704fb7cd006659376052642c16b3890458ec9a700b34f75541318060e9563353659d33b4f16ed790ecc1  importlib_metadata-5.1.0.tar.gz
"
