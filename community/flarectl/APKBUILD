# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=flarectl
pkgver=0.55.0
pkgrel=0
pkgdesc="CLI application for interacting with a Cloudflare account"
url="https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl"
arch="all"
license="BSD-3-Clause"
makedepends="go"
source="https://github.com/cloudflare/cloudflare-go/archive/refs/tags/v$pkgver/flarectl-$pkgver.tar.gz"
builddir="$srcdir/cloudflare-go-$pkgver/cmd/flarectl"
options="!check chmod-clean net" # no testsuite provided

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -buildvcs=false"

build() {
	go build -o flarectl
}

package() {
	install -Dm755 flarectl -t "$pkgdir"/usr/bin
}

sha512sums="
e045e4c3a0495717c0e3fc8e29e3f669ca9b96abd5b9baa353d5782b1269072cafb69cef7b51f0c4edda55b7e43d5a45fd4268e0a48063973981e3732de132e0  flarectl-0.55.0.tar.gz
"
