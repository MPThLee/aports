# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities
pkgver=5.100.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
pkgdesc="Core components for the KDE's Activities"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-sqlite"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	boost-dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7a50c61f3dec3f6922de038817830f00faf118ee4184a466d1913e63a33abb95b39f7bb68a6b7b95ae9cd880b86b3ed5378b923dcb5e3dc2674b8e522106fcaa  kactivities-5.100.0.tar.xz
"
