# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: August Klein <amatcoder@gmail.com>

# RESTRICTED: Any upgrade to this package must be tested against ALL architectures
# supported by Alpine. It is not allowed to mask this package on ANY architecture.
# At present, upgrade is not allowed because rustc is not yet available on ALL
# supported architectures. Any upgrade which masks an architecture WILL be reverted.

pkgname=py3-cryptography
_pyname=cryptography
pkgver=38.0.4
pkgrel=0
pkgdesc="Cryptographic recipes and primitives for Python"
url="https://cryptography.io/"
arch="all"
license="Apache-2.0 OR BSD-3-Clause"
depends="python3 py3-cffi"
makedepends="
	libffi-dev
	openssl-dev>3
	py3-gpep517
	py3-setuptools
	py3-setuptools-rust
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-hypothesis
	py3-iso8601
	py3-pretend
	py3-pytest
	py3-pytest-benchmark
	py3-pytest-subtests
	py3-pytest-xdist
	py3-tz
	"
source="https://files.pythonhosted.org/packages/source/c/cryptography/cryptography-$pkgver.tar.gz
	https://files.pythonhosted.org/packages/source/c/cryptography_vectors/cryptography_vectors-$pkgver.tar.gz
	"
builddir="$srcdir/$_pyname-$pkgver"

replaces="py-cryptography" # Backwards compatibility
provides="py-cryptography=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   3.2.2-r0:
#     - CVE-2020-36242
#   3.2.1-r0:
#     - CVE-2020-25659

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1

	# prepare cryptography vectors for testing
	cd "$srcdir/cryptography_vectors-$pkgver"
	python3 setup.py build
}

check() {
	python3 -m venv --system-site-packages test-env
	test-env/bin/python3 -m installer dist/cryptography*.whl
	(
		cd "$srcdir"/cryptography_vectors-$pkgver
		"$builddir"/test-env/bin/python3 setup.py install
	)
	test-env/bin/python3 -m pytest -n $JOBS
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/cryptography-*.whl
}

sha512sums="
2dcf3cb8208a5905b930482ce87ac56b77fda0eb02387492f0d6be9df67dc5967c93ec9a2127f83dbea1a9a3d39f1adc98497b020ad654d4afdeb67d2323afa9  cryptography-38.0.4.tar.gz
04972f13ff0217e1d8827381fafeb5071f3009d34378e60805ef1f05fefd850a0904a60caef4fc95ce396de01925a3e42443db174508627ed010ef839e97c644  cryptography_vectors-38.0.4.tar.gz
"
