# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=syndication
pkgver=5.100.0
pkgrel=0
pkgdesc="An RSS/Atom parser library"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND BSD-3-Clause"
depends_dev="qt5-qtbase-dev kcodecs-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syndication-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
cb3e9fd5f3ffa309289d475e638ff8b2d525364dce8be5ad99f9d66d52fb33844373446f73e50935f27fea46f47d2d8029566f4b3af5559adddd86986f83f6fa  syndication-5.100.0.tar.xz
"
