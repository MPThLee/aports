# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjs
pkgver=5.100.0
pkgrel=0
pkgdesc="Support for JS scripting in applications"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND BSD-3-Clause AND MIT"
depends_dev="qt5-qtbase-dev perl-dev pcre-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjs-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
b0a8547fccf45aafad0be5e41a16c56230314a7630979db37f42a183cc6b840795527374d67660aef093814e407e0fbc0a83d519281d5968f95f9b109ab6670d  kjs-5.100.0.tar.xz
"
