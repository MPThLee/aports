# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kross
pkgver=5.100.0
pkgrel=0
pkgdesc="Framework for scripting KDE applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kparts-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kross-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
eadb7807112c7b0b084d8a0b8f12df1e7a952ab31cf600c4c1edf1029b1d7f37da96a34d806ae1a141e33c9fbd5539c5574b0a0edf0c564c6c8425fe115f498f  kross-5.100.0.tar.xz
"
