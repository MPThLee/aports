# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>

pkgname=dotnet7-stage0
pkgver=7.0.100
pkgrel=1

[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""

# Tag of tarball generator.
_gittag=v7.0.100-rtm.22521.12

# Versions of prebuilt artifacts and bootstrap tar
_artifactsver=7.0.100
_bootstrapver="7.0.100"
_installerver=$_bootstrapver

# Version of packages that aren't defined in git-info or badly defined
_iltoolsver=7.0.0
_aspnetver=7.0.0

# Patches to be used. String before '_' refers to repo to patch
# Look for patch notes within each patch for what they fix / where they come from
# build_* patches applies directly to $builddir
_patches="
	aspnetcore_use-linux-musl-crossgen-on-non-x64.patch
	build_set-local-repo.patch
	installer_2780-reprodicible-tarball.patch
	installer_runtimepacks.patch
	roslyn_allow-setting-bl-location.patch
	runtime_76500-mono-musl-support.patch
	runtime_crossbuild-fixes.patch
	runtime_make-lld-use-depend-on-existing-on-target.patch
	runtime_non-portable-distrorid-fix-alpine.patch
	sdk_dummyshim-fix.patch
	"
_extra_nupkgs="https://globalcdn.nuget.org/packages/stylecop.analyzers.1.2.0-beta.435.nupkg"

_pkgver_macro=${pkgver%.*}
_pkgver_prior=1
_pkgver_name="${_pkgver_macro//[.0]}"
pkgdesc="The .NET Core stage0 bits for dotnet build"
# x86: blocked by https://github.com/dotnet/runtime/issues/77667
# armhf: blocked by https://github.com/dotnet/runtime/issues/77663
# riscv64: port WIP https://github.com/dotnet/runtime/issues/36748
# ppc64le: blocked by https://github.com/dotnet/runtime/issues/77364
arch="all !x86 !armhf !riscv64 !ppc64le"
url=https://dotnet.microsoft.com
license="MIT"
options="!check" # Testsuite in main -build aport
subpackages="
	dotnet$_pkgver_name-stage0-artifacts:artifacts:noarch
	dotnet$_pkgver_name-stage0-bootstrap
	"
source="
	https://repo.gpg.nz/apk/archives/dotnet-${_gittag/release\/}.tar.xz
	dotnet-sdk-$_bootstrapver-linux-musl-x64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-x64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm64.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm64.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-arm.noextract::https://dotnetcli.azureedge.net/dotnet/Sdk/$_bootstrapver/dotnet-sdk-$_bootstrapver-linux-musl-arm.tar.gz
	dotnet-sdk-$_bootstrapver-linux-musl-s390x.noextract::https://repo.gpg.nz/apk/archives/dotnet-sdk-$_bootstrapver-linux-musl-s390x.tar.gz
	Private.SourceBuilt.Artifacts.$_artifactsver.noextract::https://dotnetcli.azureedge.net/source-built-artifacts/assets/Private.SourceBuilt.Artifacts.$_artifactsver.tar.gz
	$_extra_nupkgs
	$_patches
	"

makedepends_host="
	alpine-release
	autoconf
	automake
	bash
	binutils
	clang
	cmake
	findutils
	g++
	gcc
	grep
	icu-dev
	krb5-dev
	libintl
	libstdc++
	libucontext-dev
	libunwind-dev
	libxml2-dev
	libxml2-utils
	linux-headers
	lttng-ust-dev
	musl-dev
	musl-utils
	openssl-dev
	pigz
	unzip
	zip
	zlib-dev
	"
case $CARCH in
	s390x|x86) ;;
	*) makedepends_host="$makedepends_host lld-dev";;
esac
makedepends_build="
	$makedepends_host
	binutils$_cross
	git
	gcc$_cross
	llvm
	llvm-dev
	python3
	xz
	"
case $CBUILD_ARCH in
	x86_64) _dotnet_arch="x64";;
	aarch64) _dotnet_arch="arm64";;
	armv7|armhf) _dotnet_arch="arm";;
	i586) _dotnet_arch="x86";;
	*) _dotnet_arch=$CBUILD_ARCH;;
esac
case $CTARGET_ARCH in
	x86_64) _dotnet_target="x64";;
	aarch64) _dotnet_target="arm64";;
	armv7|armhf) _dotnet_target="arm";;
	i586) _dotnet_target="x86";;
	*) _dotnet_target=$CTARGET_ARCH;;
esac

builddir="$srcdir"/dotnet-${_gittag/release\/}
_packagesdir="$srcdir"/local-packages
_libdir="/usr/lib"
_nugetdir="$srcdir"/nuget
_downloaddir="$srcdir"/local-downloads
_cli_root="$srcdir"/bootstrap
_nuget="$_cli_root/dotnet nuget"
_outputdir="$srcdir"/artifacts
# if true, then within pipeline environment, in which case send logs there
# to be scooped
if [ -d "$APORTSDIR/logs" ]; then
	_logdir="$APORTSDIR"/logs/Debug
else
	_logdir="$srcdir"/logs
fi

# generates tarball containing all components built by dotnet
snapshot() {
	local _pkg="$srcdir"/${builddir##*/}.tar
	ulimit -n 4096
	if [ -d "$srcdir" ]; then
		cd "$srcdir"
	else
		mkdir -p "$srcdir" && cd "$srcdir"
	fi
	if [ -d "installer" ]; then
		cd "$srcdir"/installer
	else
		git clone https://github.com/dotnet/installer && cd "$srcdir"/installer
	fi
	git checkout $_gittag

	sed 's|/src/installer||' "$startdir"/installer_2780-reprodicible-tarball.patch | patch -Np1 || true
	sed 's|/src/installer||' "$startdir"/installer_fix-version.patch | patch -Np1 || true
	if [ ! -d "$_cli_root" ]; then
		local _cli_root=
	fi

	_InitializeDotNetCli="$_cli_root" DOTNET_INSTALL_DIR="$_cli_root" DotNetBuildFromSource=true ./build.sh \
		/p:ArcadeBuildTarball=true \
		/p:TarballDir=$builddir \
		/p:TarballFilePath=$_pkg

	msg "Compressing ${builddir##*/}.tar to $SRCDEST"
	xz -T0 -9 -vv -e -c > "$SRCDEST"/${builddir##*/}.tar.xz < "$_pkg"

	cd "$startdir"
	abuild checksum
}

prepare() {
	default_prepare

	# adjusts sdk version to expected
	sed "s|7.0.100-rc.1.22431.12|$_bootstrapver|" -i "$builddir"/src/sdk/global.json
	sed "s|7.0.100-rtm.22478.12|$_bootstrapver|" -i "$builddir"/src/aspnetcore/global.json

	for i in runtime sdk installer aspnetcore roslyn; do
		sed "s|@@PACKAGESDIR@@|$_packagesdir|" -i "$builddir"/src/$i/NuGet.config
	done

	# fix sdk toolset download
	sed -i 's|$properties == \*"ArcadeBuildFromSource=true"\*|"${DotNetBuildFromSource:-}" == "true"|' "$builddir"/src/sdk/eng/restore-toolset.sh

	mkdir -p "$_cli_root"
	mkdir -p $_packagesdir $_downloaddir $_outputdir $_nugetdir $_logdir

	tar --use-compress-program="pigz" -xf "$srcdir"/dotnet-sdk-$_pkgver_macro*$_dotnet_arch.* -C "$_cli_root" --no-same-owner

	for i in $_extra_nupkgs; do
		$_nuget push "$srcdir"/${i##*/} --source="$_packagesdir"
	done
}

_runtime() {
	if [ -z "${_runtimever+x}" ]; then
		local _runtimever=$(grep OutputPackageVersion "$builddir"/git-info/runtime.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _runtimever_ns=$(awk '{if($2 ~ "Name=\"VS.Redist.Common.NetCore.SharedFramework.x64.*\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building runtime version $_runtimever"
	cd "$builddir"/src/runtime

	local args="
		$_globalargs
		-clang
		-arch $_dotnet_target
		-bl:$_logdir/$pkgname-runtime.binlog
		/p:NoPgoOptimize=true
		/p:EnableNgenOptimization=false
		/p:GitCommitHash=$(cat ./.git/HEAD)
		"
	if [ "$CBUILD" != "$CHOST" ]; then
		local args="$args -cross"
	fi
	if [ "$_runtimever" != "${_runtimever##*-}" ]; then
		local args="$args /p:VersionSuffix=${_runtimever##*-}"
	fi
	ROOTFS_DIR="$CBUILDROOT" ./build.sh $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/Runtime/$_runtimever_ns
	cp artifacts/packages/*/*/dotnet-runtime-*-*.tar.gz $_downloaddir/Runtime/$_runtimever_ns
}

_roslyn() {
	if [ -z "${_roslynver+x}" ]; then
		local _roslynver=$(grep OutputPackageVersion "$builddir"/git-info/roslyn.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _roslynver_ns=$(awk '{if($2 == "Name=\"Microsoft.Net.Compilers.Toolset\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building roslyn version $_roslynver"
	cd "$builddir"/src/roslyn

	local args="
		$_globalargs
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/bl:$_logdir/$pkgname-roslyn.binlog
		"
	if [ "$_roslynver" != "${_roslynver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_roslynver##*-}"
	fi
	DotNetBuildFromSource=false ./eng/build.sh --restore $args /p:UseAppHost=false
	./eng/build.sh --restore --build --pack $args
	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
}

_sdk() {
	if [ -z "${_sdkver+x}" ]; then
		local _sdkver=$(grep OutputPackageVersion "$builddir"/git-info/sdk.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _sdkver_ns=$(awk '{if($2 == "Name=\"Microsoft.NET.Sdk\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Building sdk version $_sdkver"
	cd "$builddir"/src/sdk

	local args="
		$_globalargs
		-bl:$_logdir/$pkgname-sdk.binlog
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:Architecture=$_dotnet_target
		"
	if [ "$_sdkver" != "${_sdkver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_sdkver##*-}"
	fi

	# ArgumentsReflector doesn't build correctly when built from source
	DotNetBuildFromSource=false dotnet build src/Tests/ArgumentsReflector/ArgumentsReflector.csproj $args
	./build.sh --pack $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/Sdk/$_sdkver_ns
	cp artifacts/packages/*/*/dotnet-toolset-internal-*.zip "$_downloaddir"/Sdk/$_sdkver_ns
}

_aspnetcore() {
	if [ -z "${_aspnetver+x}" ]; then
		local _aspnetver=$(grep OutputPackageVersion "$builddir"/git-info/aspnetcore.props | sed -E 's|</?OutputPackageVersion>||g' | tr -d ' ')
	fi
	local _aspnetver_ns=$(awk '{if($2 == "Name=\"Microsoft.AspNetCore.App.Ref.Internal\""){print $3}}' "$builddir"/src/installer/eng/Version.Details.xml | awk -F '"' '{print $2}')

	msg "[$(date)] Build aspnetcore version $_aspnetver"
	cd "$builddir"/src/aspnetcore
	local args="
		$_globalargs
		--os-name linux-musl
		-arch $_dotnet_target
		-no-build-nodejs
		-bl:$_logdir/$pkgname-aspnetcore.binlog
		/p:BuildNodeJs=false
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:DotNetAssetRootUrl=file://$_downloaddir/
		/p:EnablePackageValidation=false
		"
	if [ "$_dotnet_target" = "x86" ] || [ "$_dotnet_target" = "ppc64le" ]; then
		local args="$args /p:CrossgenOutput=false"
	fi
	if [ "$_aspnetver" != "${_aspnetver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_aspnetver##*-}"
	fi
	./eng/build.sh --pack $args

	for i in artifacts/packages/*/*/*.nupkg; do
		$_nuget push $i --source="$_packagesdir"
	done
	mkdir -p "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore-runtime-internal-*-linux-musl-$_dotnet_target.tar.gz "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
	cp artifacts/installers/*/aspnetcore_base_runtime.version "$_downloaddir"/aspnetcore/Runtime/$_aspnetver_ns
}

_installer() {
	msg "[$(date)] Building installer version $_installerver"
	cd "$builddir"/src/installer

	local args="
		$_globalargs
		-bl:$_logdir/$pkgname-installer.binlog
		/p:OSName=linux-musl
		/p:HostOSName=linux-musl
		/p:Architecture=$_dotnet_target
		/p:CoreSetupBlobRootUrl=file://$_downloaddir/
		/p:DotnetToolsetBlobRootUrl=file://$_downloaddir/
		/p:GitCommitHash=$(cat ./.git/HEAD)
		/p:GitCommitCount=$(grep GitCommitCount "$builddir"/git-info/installer.props | sed -E 's|</?GitCommitCount>||g' | tr -d ' ')
		/p:PublicBaseURL=file://$_downloaddir/
		"
	if [ "$_installerver" != "${_installerver##*-}" ]; then
		local args="$args /p:VersionSuffix=${_installerver##*-}"
	fi
	if [ "$_dotnet_target" = "x86" ]; then
		local args="$args /p:DISABLE_CROSSGEN=True"
	fi
	./build.sh $args

	mkdir  -p "$_downloaddir"/installer/$_installerver
	cp artifacts/packages/*/*/dotnet-sdk-$_pkgver_macro*.tar.gz "$_downloaddir"/installer/$_installerver
}

build() {
	export _InitializeDotNetCli=$_cli_root
	export DOTNET_INSTALL_DIR=$_cli_root
	export PATH="$_cli_root:$PATH"
	export NUGET_PACKAGES=$_nugetdir
	export DotNetBuildFromSource=true
	export DOTNET_CLI_TELEMETRY_OPTOUT=true
	export DOTNET_SKIP_FIRST_TIME_EXPERIENCE=true
	export SHELL=/bin/bash
	export EXTRA_CPPFLAGS="${CPPFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_CXXFLAGS="${CXXFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_CFLAGS="${CFLAGS/--sysroot=$CBUILDROOT}"
	export EXTRA_LDFLAGS="$LDFLAGS"
	unset CXXFLAGS CFLAGS LDFLAGS CPPFLAGS

	ulimit -n 4096

	_globalargs="-c Release"

	# Parallel restore is broken on mono-based builds since dotnet7
	# see https://github.com/dotnet/runtime/issues/77364
	case $CARCH in
		s390x|ppc64le)_globalargs="$_globalargs /p:RestoreDisableParallel=true";;
	esac

	_runtime
	_roslyn
	_sdk
	_aspnetcore
	_installer
}

package() {
	# lua-aports / buildrepo doesn't know to always build stage0 first when dealing
	# with virtual packages. Thus, we need to depend on an empty stage0 pkg that
	# dotnetx-build will pull, thus forcing build of stage0 first
	mkdir -p "$pkgdir"
}

bootstrap() {
	# allows stage0 to be pulled by dotnetx-build if first build of dotnetx
	provides="dotnet$_pkgver_name-bootstrap"
	provider_priority=$_pkgver_prior

	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/docs \
		"$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/comp \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver

	# unpack build artifacts to bootstrap subdir for use by future builds
	tar --use-compress-program="pigz" \
		-xf "$_downloaddir"/installer/$_installerver/dotnet-sdk-$_pkgver_macro*.tar.gz \
		-C "$subpkgdir"/$_libdir/dotnet/bootstrap/$pkgver/ \
		--no-same-owner

	local _iltoolsArray="
		runtime.*.Microsoft.NETCore.TestHost.*.nupkg
		runtime.*.Microsoft.NETCore.ILAsm.*.nupkg
		runtime.*.Microsoft.NETCore.ILDAsm.*.nupkg
		"

	local _nupkgsArray="
		$_iltoolsArray
		Microsoft.NETCore.App.Host.*.*.nupkg
		Microsoft.NETCore.App.Runtime.*.*.nupkg
		Microsoft.NETCore.App.Crossgen2.*.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHost.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHostPolicy.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetHostResolver.*.nupkg
		runtime.*.Microsoft.NETCore.DotNetAppHost.*.nupkg
		Microsoft.AspNetCore.App.Runtime.linux-musl-*.*.nupkg
		"

	# copies artifacts to artifacts dir for use by future dotnet builds
	for i in $_nupkgsArray; do install -Dm644 "$_packagesdir"/$i "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/ || true; done
	for i in $_extra_nupkgs; do install -Dm644 "$srcdir"/${i##*/} "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/; done

	msg "Changing iltools version to $_iltoolsver"
	# source-build expects a certain version of ilasm, ildasm and testhost
	# following adjusts version
	for i in $_iltoolsArray; do
		local nupath="$subpkgdir"$_libdir/dotnet/artifacts/$pkgver/$i
		local nupath=$(find $nupath || true)
		local nupkg="${nupath##*/}"
		local nuname="${nupkg/.nupkg}"
		if [ -z "${nuname/*rtm*}" ] || [ -z "${nuname/*servicing*}" ]; then
			nuname=${nuname%-*}
		fi
		local nuname="${nuname%.*.*.*}"
		local nuver="${nupkg/$nuname.}"
		local nuver="${nuver/.nupkg}"
		local nuspec="$nuname.nuspec"
		if [ ! "$nupath" ] || [ "$nupath" = "${nupath/$nuver/$_iltoolsver}" ]; then
			continue
		fi
		# shellcheck disable=SC2094
		unzip -p "$nupath" $nuspec | sed "s|$nuver|$_iltoolsver|" > "$srcdir"/$nuspec
		cd "$srcdir"
		zip -u "$nupath" $nuspec
		mv "$nupath" "${nupath/$nuver/$_iltoolsver}"
	done
}

# build relies on a plethora of nupkgs which are provided by this Artifacts file.
# stage0 sources these from Microsoft, which then allows bootstrap to build
# locally hosted versions. The following unpacks built tarball into directory
# for use by future builds.
artifacts() {
	pkgdesc="Internal package for building .NET $_pkgver_macro Software Development Kit"
	# hack to allow artifacts to pull itself
	provides="dotnet$_pkgver_name-bootstrap-artifacts"
	provider_priority=$_pkgver_prior

	# directory creation
	install -dm 755 \
		"$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver \
		"$subpkgdir"/usr/share/licenses

	# extract artifacts to artifacts dir for use by future dotnet builds
	tar --use-compress-program="pigz" \
		-xf "$srcdir"/Private.SourceBuilt.Artifacts.*.noextract \
		-C "$subpkgdir"/$_libdir/dotnet/artifacts/$pkgver/ \
		--no-same-owner \
		--exclude '*x64*'
}

sha512sums="
1f0e7e6efb7076bc7d5a11e0589c27258cfe2bfa6d81a6d41f172c7292dab60f62b3e95e3873d4ee024d765f09507851d169a35ae06e75ce815d76717adb5025  dotnet-v7.0.100-rtm.22521.12.tar.xz
2ee0a055a3e46c6d9ced3cada5f91141b3966e76f4c4b11e58cd4c89ea69408a5b0efaaa21aaa04f743add38f1435f5a5852271a4222d5cd858907ec44f0af2e  dotnet-sdk-7.0.100-linux-musl-x64.noextract
8b3e95cc3e80eb05c7a0bc7ede1033320e03c78f4ecb7cc99b85fa99f56d72bc06342f342fe957e4aadacf9fb83fff15e658ae62c8fa8b29051898929c5ea833  dotnet-sdk-7.0.100-linux-musl-arm64.noextract
26b7ca079c8c2bafb32b5794de698dd325837897ee4120d0a7bbbcdc7f034de5031c6f30536866ccd7d1338625f937f41cfa5524c64163faded58846bfa674af  dotnet-sdk-7.0.100-linux-musl-arm.noextract
e4882126f6c15d00aa2ca4cab501b682e8d5d92ce3280fd2c955b5b0a248f66a6c893fd7397e9792b071e10bc62aeceffde4f865847dcf9d8b5fec1a2343fccd  dotnet-sdk-7.0.100-linux-musl-s390x.noextract
cafb24303cdf92cdc8e2c703aa81684001fecf4a6f609a8008fd32ded9e06552bc04e38e21b6c26b0cf899686a9d05f6fad26a98bc73bd5d6347d682eec275a8  Private.SourceBuilt.Artifacts.7.0.100.noextract
2ede8d9352a51861a5b2550010ff55da8241381a6fa6cc49e025f1c289b230b8c0177e93850de4ea8b6f702c1f2d50d81a9f4d890ca9441c257b614f2a5e05dd  stylecop.analyzers.1.2.0-beta.435.nupkg
4394debb3cfec7bec7f223029c8283fd57b28ad765d47f354c7ca667756cbfc1ae4a51b216a0f81452400f60b58f08347bac327cd717a1947dad737b37cbe3ef  aspnetcore_use-linux-musl-crossgen-on-non-x64.patch
97d3194f8ad96521002e3693261451154d6ba567ea807e9daa4d55e1cda0693791fab4443e3d0d0fa8bc51ba53dd9b4077c723fa8f1f34cdd1661df828ce4227  build_set-local-repo.patch
e68be02038672a3d2a2f539b1c7309b055c3c5256f5e4c9d24edbc203df5b4358b1759fe3626fbefd234182817fa1018e574ab64093dab90e60272d686e8e05f  installer_2780-reprodicible-tarball.patch
1fe84c55f63c3b32fe7c58aa23ba25e2e9ddeee21acd664947206b8a9678bf38e2c1a95252243d7981dd9423bda49f40dd463fa6e619fe7f329a31794829ee88  installer_runtimepacks.patch
f2073b2713a32d36ff52a4b37170b00fd646b42e1dad4c268afd9b478d2457df0aa565006b6e2f68385319ebeaf147ed92700ae28490f73a0f512681362a3ce8  roslyn_allow-setting-bl-location.patch
37767ac200d95598e35acb8e9850bef5291e971991551a27014e95ae1f634ed6b6293dd2d83dedc63509c9d62024bf3b853004d5b9ae0b3fe71f31c1109e11ec  runtime_76500-mono-musl-support.patch
de674edb37789fd8f30ff0085adcc57f3b5e4be752be9f60de0ad22ddd2ef3deefe71f58c0f6e785f1d480658c608f0da1818113590d3f92b50b82b495ff3cc8  runtime_crossbuild-fixes.patch
3624e14029e4fd5ad0f3dafb6b9b5e5efa8268a25083d8af8119ac680663a9278658c96d8926892e9f65680af5be13da0be51c82f68a99700fe1456a2765da2b  runtime_make-lld-use-depend-on-existing-on-target.patch
8be4b028fb15d4bc0ba34711f7491c49b4908e017a9c46fcd3817c59aa1459c0217dca405f46cfaf5e115ad0fbcd60d2593c05c6e1da2632612bf6179867cd10  runtime_non-portable-distrorid-fix-alpine.patch
b5f28aa8bdbd24c589276a60893859965eb78b0abfbdac7a6e86715ac87e3bbd8bc271852ff62a41ea83b53af02e1c1814abcccc8862c07b3ac74633d1be0a32  sdk_dummyshim-fix.patch
"
