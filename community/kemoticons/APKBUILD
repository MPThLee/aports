# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kemoticons
pkgver=5.100.0
pkgrel=0
pkgdesc="Support for emoticons and emoticons themes"
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kservice-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# kemoticons-kemoticontest and kemoticons-ktexttohtmlplugintest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kemoticons-(kemoticon|ktexttohtmlplugin)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a1d22009dd6035e2735c0360f675eb9ed6021617213986574a3821b25b3e57143b3b8b3388b61e2dad75987679903569a640069555a039f8598306d6df0c2120  kemoticons-5.100.0.tar.xz
"
