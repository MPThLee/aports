# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=java-postgresql-jdbc
_pkgname=postgresql-jdbc
pkgver=42.5.0
pkgrel=0
pkgdesc="Java JDBC 4.2 (JRE 8+) driver for PostgreSQL database"
url="https://jdbc.postgresql.org/"
# riscv64 blocked by java-jdk
arch="noarch !riscv64"
license="BSD-3-Clause"
makedepends="java-jdk gradle"
source="$pkgname-$pkgver.tar.gz::https://github.com/pgjdbc/pgjdbc/archive/REL$pkgver.tar.gz"
options="!check net" # tests require running postgres server
builddir="$srcdir/pgjdbc-REL$pkgver/pgjdbc"

# secfixes:
#   42.4.2-r0:
#     - CVE-2022-31197
#   42.2.25-r0:
#     - CVE-2022-21724
#     - CVE-2020-13692

build() {
	# Note: Gradle downloads quite many dependencies, but
	# these are used only for building, not bundled to the final JAR.
	export JAVA_HOME="/usr/lib/jvm/default-jvm"
	gradle jar
}

check() {
	gradle test
}

package() {
	install -Dm644 ./build/libs/postgresql-$pkgver-SNAPSHOT.jar \
		"$pkgdir"/usr/share/java/$_pkgname-$pkgver.jar
	ln -s $_pkgname-$pkgver.jar "$pkgdir"/usr/share/java/$_pkgname.jar
}

sha512sums="
cc24d1e03b500f1a26b8bbe63eb38ec54bcd454ed726a9dd68a909b25c5e7c542c5448a498f99062e75e22c6605e58fa210441ab03885a80238e9f9402ee01f0  java-postgresql-jdbc-42.5.0.tar.gz
"
