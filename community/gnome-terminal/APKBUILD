# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-terminal
pkgver=3.46.6
pkgrel=0
pkgdesc="GNOME terminal emulator application"
url="https://wiki.gnome.org/Apps/Terminal"
# gnome-shell
arch="all !armhf !s390x !riscv64"
license="GPL-2.0-or-later AND GFDL-1.3-only"
depends="
	dbus
	desktop-file-utils
	gnome-shell-schemas
	gsettings-desktop-schemas
	ncurses-terminfo-base
	"
makedepends="
	dconf-dev
	glib-dev
	gnome-shell
	gsettings-desktop-schemas-dev
	intltool
	itstool
	meson
	nautilus-dev
	pcre2-dev
	vte3-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://gitlab.gnome.org/GNOME/gnome-terminal/-/archive/$pkgver/gnome-terminal-$pkgver.tar.gz
	fix-W_EXITCODE.patch
	"

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
d3c27b6d42ca7c6425755939c1d6956b1e91c90414536ffa200b4eb0c4633f9ff4933e333e27a5a7f5b18f3ff0b954515b11075a40f4feb8893c44049f218fb7  gnome-terminal-3.46.6.tar.gz
3bbc0e9dbbbfd4a3263b5d195a78df9cc70d13ae02d3a2981886001e79d92c2bfe4962954efba8fbab69f23e1778d5fbe3436f494b70e196ecedb02bf9d95400  fix-W_EXITCODE.patch
"
