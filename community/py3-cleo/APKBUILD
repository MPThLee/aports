# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-cleo
pkgver=1.0.0
pkgrel=0
pkgdesc="Python3 library to create beautiful and testable command-line interfaces"
url="https://github.com/python-poetry/cleo"
arch="noarch"
license="MIT"
depends="python3 py3-crashtest py3-rapidfuzz"
makedepends="py3-gpep517 py3-poetry-core"
checkdepends="py3-pytest py3-pytest-mock"
source="$pkgname-$pkgver.tar.gz::https://github.com/python-poetry/cleo/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/cleo-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m installer -d testenv \
		dist/cleo-$pkgver-py3-none-any.whl
	local sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	PYTHONPATH="$PWD/testenv/$sitedir" python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/cleo-$pkgver-py3-none-any.whl
}

sha512sums="
a45af3f4ff0fc9535363bdcf265ab3d7726af99cf4b48e896f9d5fb1404721b4641f80e37a7266690a1f7d4a261cbd4c746338294ff29745f782f8657cd2bb07  py3-cleo-1.0.0.tar.gz
"
