# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=gojq
pkgver=0.12.10
pkgrel=0
pkgdesc="Pure Go implementation of jq"
url="https://github.com/itchyny/gojq"
license="MIT"
arch="all"
makedepends="go"
subpackages="$pkgname-zsh-completion"
source="https://github.com/itchyny/gojq/archive/v$pkgver/gojq-$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build ./cmd/gojq
}

check() {
	go test ./...
}

package() {
	install -Dm755 gojq -t "$pkgdir"/usr/bin/

	install -Dm644 _gojq -t "$pkgdir"/usr/share/zsh/site-functions/
}

sha512sums="
c3d0ca7c0d379953968714da17dff43b5690b7ab27d1f6d7955f0da4cb594677c67d8de9c6b4f25e64e9de0f1cf2ab57e77333d1489be0085fa3616cef99c207  gojq-0.12.10.tar.gz
"
