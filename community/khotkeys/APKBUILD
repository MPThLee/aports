# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=khotkeys
pkgver=5.26.4
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by libksysguard
arch="all !armhf !s390x !riscv64"
url="https://kde.org/plasma-desktop/"
pkgdesc="Key Accelerator Application"
license="GPL-2.0-only AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/khotkeys-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
193d7001baf6f2f9fa50be18e40c149e3033d3c9d256f8e2c8b8cb5a1536c630f04d3229827217348c48f81354dda8757400c8e3185cebc027121b2b442cf248  khotkeys-5.26.4.tar.xz
"
