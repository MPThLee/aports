# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktextwidgets
pkgver=5.100.0
pkgrel=0
pkgdesc="Advanced text editing widgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	ki18n-dev
	kiconthemes-dev
	kservice-dev
	kwidgetsaddons-dev
	qt5-qtspeech-dev
	sonnet-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/ktextwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9a9f1bf0541767702afdb1e704e7554abab8288a0f84553caa1fd8cdf0e025a28acdb4bf7168a1d51e344f8044b0b69a6a8072cdecbe9b917c92bd5a601f755f  ktextwidgets-5.100.0.tar.xz
"
