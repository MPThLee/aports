# Contributor: Alex Yam <alex@alexyam.com>
# Maintainer: Alex Yam <alex@alexyam.com>
pkgname=cherrytree
pkgver=0.99.51
pkgrel=0
pkgdesc="hierarchical note taking application featuring rich text and syntax highlighting"
url="https://github.com/giuspen/cherrytree"
license="GPL-3.0-or-later"
# s390x: libxml++2.6 missing
arch="all !s390x"
makedepends="
	cmake
	curl-dev
	fmt-dev
	gspell-dev
	gtkmm3-dev
	gtksourceviewmm3-dev
	libxml++-2.6-dev
	python3
	samurai
	spdlog-dev
	sqlite-dev
	uchardet-dev
	vte3-dev
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
source="https://github.com/giuspen/cherrytree/releases/download/$pkgver/cherrytree_$pkgver.tar.xz"
builddir="$srcdir/${pkgname}_$pkgver"

case "$CARCH" in
	riscv64) options="textrels" ;;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON \
		-DAUTO_RUN_TESTING=OFF \
		-DPYTHON_EXEC=/usr/bin/python3
	cmake --build build
}

check() {
	# Skip broken ExportsMultipleParametersTests.ChecksExports tests
	#
	# (run_tests_with_x_1:7077): Gtk-WARNING **: 04:05:18.849:
	#   Found an icon but could not load it. Most likely gdk-pixbuf does not
	#   provide SVG support.
	#
	# [  FAILED  ] ExportsTests/ExportsMultipleParametersTests.ChecksExports
	#   /testing/cherrytree/src/cherrytree_0.99.44/tests/data_данные
	#   /test_документ.ctb", "--export_to_html_dir")
	xvfb-run -a ctest --test-dir build -j $JOBS --output-on-failure \
		-E ExportsMultipleParametersTests.ChecksExports
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install

	# Remove gmock/gtest files
	rm -rv "$pkgdir"/usr/include "$pkgdir"/usr/lib

	# bundled breeze-icons
	rm -rv "$pkgdir"/usr/share/cherrytree/icons
}

sha512sums="
98bcbfcb578970c9709f96926b0a9f5100ffe620453519e4292620fe9526c9134ac72f399703b045e4a433565d8a05e0f8fde126f48d458e7f9edb7b79ee34e6  cherrytree_0.99.51.tar.xz
"
